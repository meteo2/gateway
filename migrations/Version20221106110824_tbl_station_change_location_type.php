<?php

declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20221106110824_tbl_station_change_location_type extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE station ALTER location TYPE Geometry(point) USING location::geometry');
        $this->addSql('COMMENT ON COLUMN station.created_at IS \'(DC2Type:datetimetz_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('ALTER TABLE station ALTER location TYPE point');
    }
}
