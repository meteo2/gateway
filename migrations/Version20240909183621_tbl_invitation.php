<?php

declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20240909183621_tbl_invitation extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on "postgresql".'
        );

        $this->addSql(/** @lang PostgreSQL */ <<<SQL
            CREATE TABLE invitation (
                token text not null primary key,
                used_by uuid,
                created_at timestamptz not null default current_timestamp(0),
                updated_at timestamptz
            )
        SQL);

        $this->addSql( <<<SQL
            COMMENT ON TABLE invitation IS 'List of generated invitation tokens and who used it to register'
        SQL);

        $this->addSql('COMMENT ON COLUMN invitation.created_at IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN invitation.updated_at IS \'(DC2Type:datetimetz_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on "postgresql".'
        );

        $this->addSql(/** @lang PostgreSQL */<<<SQL
            DROP TABLE invitation;
        SQL);

    }
}
