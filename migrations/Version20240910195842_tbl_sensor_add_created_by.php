<?php

declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20240910195842_tbl_sensor_add_created_by extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on "postgresql".'
        );

        $this->addSql(/** @lang PostgreSQL */ <<<SQL
            ALTER TABLE station
                ADD COLUMN created_by UUID;
        SQL);

        $this->addSql(/** @lang PostgreSQL */ <<<SQL
            CREATE INDEX station_created_by_idx ON station (created_by);
        SQL);


    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on "postgresql".'
        );

        $this->addSql(/** @lang PostgreSQL */<<<SQL
            ALTER TABLE station
                DROP COLUMN created_by;
        SQL);

    }
}
