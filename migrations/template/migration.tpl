<?php

declare(strict_types = 1);

namespace <namespace>;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class <className> extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on "postgresql".'
        );

        $this->addSql(/** @lang PostgreSQL */ <<<SQL

        SQL);
<up>
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            !$this->connection->getDatabasePlatform() instanceof PostgreSQLPlatform,
            'Migration can only be executed safely on "postgresql".'
        );

        $this->addSql(/** @lang PostgreSQL */<<<SQL

        SQL);
<down>
    }
}
