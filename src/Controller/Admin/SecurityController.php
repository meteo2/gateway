<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\Security\RegisterFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route('/admin/login', name: 'app_admin_login')]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('admin/login/index.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    #[Route('/admin/logout', name: 'app_admin_logout', methods: ['GET'])]
    public function logout(): Response
    {
        throw new \Exception("Don't forget to activate logout in security.yml");
    }

    #[Route('/admin/register', name: 'app_admin_register')]
    public function register(Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $hasher): Response
    {
        $form = $this->createForm(RegisterFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = new User();
            $user->setEmail($form->get('email')->getData());

            $plainPassword = $form->get('password')->getData();
            // TODO: Instead of setting the password directly send an email with a link to set the password
            $user->setPassword($hasher->hashPassword($user, $plainPassword));

            try {
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Your account has been created.');

                return $this->redirectToRoute('app_admin_login');
            } catch (\Exception $e) {
                $this->addFlash('error', 'There was an error creating your account.');
                return $this->redirectToRoute('app_admin_register');
            }
        }

        return $this->render('admin/register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}
