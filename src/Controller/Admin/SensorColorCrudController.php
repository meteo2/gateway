<?php

namespace App\Controller\Admin;

use App\Entity\SensorColor;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SensorColorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SensorColor::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
