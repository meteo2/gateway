<?php

namespace App\Controller\Admin;

use App\Entity\StationData;
use App\Field\SensorsField;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class StationDataCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StationData::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('station'),
//            ArrayField::new('value'),
//            SensorsField::new('value'),
            DateTimeField::new('created')
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(['created' => 'DESC'])
            ->setPaginatorPageSize(10)
            ->showEntityActionsInlined()
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
            ->disable(Action::NEW, Action::DELETE, Action::EDIT)
        ;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $qb = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $qb->innerJoin('entity.station', 'station');

        if (!$this->isGranted('ROLE_ADMIN')) {
            $qb->andWhere('station.createdBy = :user');
            $qb->setParameter('user', $this->getUser());
        }

        return $qb;
    }


}
