<?php

namespace App\Controller;

use App\Repository\SensorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SensorController extends AbstractController
{
    #[Route('/sensors', name: 'app_sensor')]
    public function index(SensorRepository $sensorRepository): Response
    {
        return $this->json($sensorRepository->findAllWithColors());
    }
}
