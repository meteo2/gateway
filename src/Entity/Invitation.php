<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[
    ORM\Entity(),
    ORM\Table(name: 'invitation'),
    ORM\HasLifecycleCallbacks(),
]
class Invitation
{
    #[
        ORM\Id,
        ORM\Column(type: 'string', length: 6),
    ]
    private string $token;

    #[
        ORM\Column(type: 'string', length: 255, nullable: true),
    ]
    private ?string $usedBy;

    #[
        ORM\Column(type: 'datetimetz_immutable'),
    ]
    private \DateTimeImmutable $createdAt;

    #[
        ORM\Column(type: 'datetimetz_immutable', nullable: true),
    ]
    private ?\DateTimeImmutable $updatedAt = null;

    public function __construct(string $token, ?string $usedBy)
    {
        $this->token = $token;
        $this->usedBy = $usedBy;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getUsedBy(): ?string
    {
        return $this->usedBy;
    }

    public function setUsedBy(?string $usedBy): void
    {
        $this->usedBy = $usedBy;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    #[ORM\PreUpdate]
    public function preUpdate(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
