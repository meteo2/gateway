<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
class SensorColor
{
    #[
        ORM\Id,
        ORM\ManyToOne(targetEntity: Sensor::class, inversedBy: 'colors'),
        ORM\JoinColumn(name: 'code', referencedColumnName: 'code')
    ]
    private string $code;

    #[ORM\Column(name: 'value', type: 'float')]
    private float $value;

    #[ORM\Column()]
    private ?string $color = null;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
}
