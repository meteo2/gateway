<?php
declare(strict_types=1);

namespace App\Form\Security;

use App\Entity\Invitation;
use App\Validator\Constraints\EntityExists;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Sequentially;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', TextType::class, [
                'constraints' => [
                    new Sequentially([
                        new NotBlank(),
                        new Length(['min' => 6, 'max' => 6]),
                        new EntityExists([
                            'entityClass' => Invitation::class,
                            'fields' => ['token'],
                            'message' => 'There is no invitation with this code.',
                        ]),
                    ]),
                ],
            ])
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
        ;
    }
}
