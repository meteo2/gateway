<?php
declare(strict_types=1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::TARGET_PROPERTY)]
class EntityExists extends Constraint
{
    public const NOT_FOUND_ERROR = 'd4b3b3b3-6b9b-41cd-a99e-4844bcf3077f';

    protected const ERROR_NAMES = [
        self::NOT_FOUND_ERROR => 'NOT_FOUND_ERROR',
    ];

    public string $message = 'This value does not exist.';

    public string $entityClass;

    public array $fields = [];

    public function validatedBy(): string
    {
        return 'entity_exists';
    }
}
